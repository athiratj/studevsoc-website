echo DEBUG=0 >> .env.prod

echo ALLOWED_HOSTS=$ALLOWED_HOSTS >> .env.prod

echo SECRET_KEY=$SECRET_KEY >> .env.prod

echo EMAIL_BACKEND=$EMAIL_BACKEND >> .env.prod

echo SQL_ENGINE=django.db.backends.postgresql >> .env.prod
echo DATABASE=postgres >> .env
echo SQL_DATABASE=$SQL_DATABASE >> .env.prod
echo SQL_USER=$SQL_USER >> .env.prod
echo SQL_PASSWORD=$SQL_PASSWORD >> .env.prod
echo SQL_HOST=$SQL_HOST >> .env.prod
echo SQL_PORT=$SQL_PORT >> .env.prod
echo DATABASE_URL=$DATABASE_URL >> .env.prod

echo WEB_IMAGE=$IMAGE:web  >> .env.prod
echo NGINX_IMAGE=$IMAGE:nginx  >> .env.prod
echo CI_DOCKER_DEPLOY_USER=$CI_DOCKER_DEPLOY_USER   >> .env.prod
echo CI_DOCKER_DEPLOY_KEY=$CI_DOCKER_DEPLOY_KEY  >> .env.prod
echo CI_REGISTRY=$CI_REGISTRY  >> .env.prod
echo IMAGE=$CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME >> .env.prod


echo POSTGRES_USER=$POSTGRES_USER >> .env.prod.db
echo POSTGRES_PASSWORD=$POSTGRES_PASSWORD >> .env.prod.db
echo POSTGRES_DB=$POSTGRES_DB >> .env.prod.db
