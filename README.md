Source Code for studevsoc.com
===
[![pipeline status](https://gitlab.com/studevsoc/studevsoc-website/badges/master/pipeline.svg)](https://gitlab.com/studevsoc/studevsoc-website/-/commits/master)

- Stack: 
    - Django
    - Basic Web Dev Tech: HTML, CSS and JS
- About Us:
We are a community with a vision to bridge the gap between students and professionals. Our mission is to make students industry ready by helping them achieve professionalism. We actively conduct workshops and interactive sessions to help aid us in fulfilling our mission. We often collaborate with other communities to bring events such as the first ever Rails Girls Kochim, Debutsav Kerala, LetsPy etc. We are turning three and so far, the journey has been sweet. 